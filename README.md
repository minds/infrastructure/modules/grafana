# grafana

| Node Group | Deployment | Environments | Namespace |
| ---------- | ---------- | ------------ | --------- |
| stateless  | Argo CD    | Production   | grafana   |

## Description

Helm chart for Grafana. Used to visualize metrics from various data sources and alarm when conditions are met.

***Access at https://grafana.minds.com***

## Deploy

Deployed using Argo CD. View the application in Argo, and "Sync" when changes are in master.
